package id.kotlin.dagger.features

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import dagger.android.AndroidInjection
import id.kotlin.dagger.R
import javax.inject.Inject

class FiturSatuActivity : AppCompatActivity() {

    @Inject lateinit var fiturSatu: FiturSatu

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fitur_satu)
        AndroidInjection.inject(this)
        fiturSatu.createSomething()

        val result = fiturSatu.tambah(5, 5)
        Toast.makeText(this, "$result", Toast.LENGTH_LONG).show()
    }
}