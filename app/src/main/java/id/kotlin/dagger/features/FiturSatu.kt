package id.kotlin.dagger.features

import android.util.Log

class FiturSatu {

    fun createSomething() {
        Log.d("FITUR_SATU", "Bikin sesuatu")
    }

    fun tambah(x: Int, y: Int): Int = x.plus(y)
}