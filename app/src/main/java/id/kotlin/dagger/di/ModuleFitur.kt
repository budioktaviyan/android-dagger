package id.kotlin.dagger.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import id.kotlin.dagger.features.FiturSatuActivity

@Module
abstract class ModuleFitur {

    @PerFeature
    @ContributesAndroidInjector()
    @Suppress()
    abstract fun bindsFiturSatuActivity(): FiturSatuActivity
}