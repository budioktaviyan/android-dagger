package id.kotlin.dagger.di

import android.app.Application
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import id.kotlin.dagger.App
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AndroidInjectionModule::class,
    ModuleUtama::class,
    ModuleFitur::class
])
interface KomponenUtama {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        fun build(): KomponenUtama
    }

    fun inject(application: App)
}