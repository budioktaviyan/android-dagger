package id.kotlin.dagger.di

import dagger.Module
import dagger.Provides
import id.kotlin.dagger.features.FiturSatu
import javax.inject.Singleton

@Module
class ModuleUtama {

    @Provides
    @Singleton
    fun providesFiturSatu(): FiturSatu = FiturSatu()
}